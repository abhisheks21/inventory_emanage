package com.abhishek.Inventory_emanage.services;

import com.abhishek.Inventory_emanage.domain.entities.Inventory;
import com.abhishek.Inventory_emanage.domain.entities.Order;
import com.abhishek.Inventory_emanage.domain.repositories.InventoryRepository;
import com.abhishek.Inventory_emanage.domain.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    InventoryRepository inventoryRepository;

    public List<Order> getAllOrders(){
        return orderRepository.findAll();
    }

    public String handlePurchaseOrder(Order order){
        List<List<String>> productList = order.getProductList();
        for (List<String> product : productList) {

            String prodId = product.get(0);
            int quantity = Integer.parseInt(product.get(1));
            double price = Double.parseDouble(product.get(2));
            double mrp = Double.parseDouble(product.get(3));
            double discountPercent = Double.parseDouble(product.get(4));

            // check if there is same product in inventory
            // with same price
            List<Inventory> similarInv = inventoryRepository.findByProductIdAndPurchasePrice(prodId,price);

            if(similarInv.isEmpty()){
                // Adds new inventory item
                Inventory inventoryToAdd = new Inventory(prodId,quantity,price, mrp, discountPercent);
                inventoryToAdd.setSellingPrice(mrp - (mrp*discountPercent/100));
                inventoryToAdd.setUpdatedAt(new Timestamp(new Date().getTime()).toString());
                inventoryRepository.insert(inventoryToAdd);
                System.out.println("inventory added for product ID " + inventoryToAdd.getProductId());
            } else{
                // Update inventory record by adding quantiy
                Inventory invTOUpdate = similarInv.get(0);
                invTOUpdate.setQuantity(invTOUpdate.getQuantity() + quantity);
                invTOUpdate.setUpdatedAt(new Timestamp(new Date().getTime()).toString());
                inventoryRepository.save(invTOUpdate);
                System.out.println("Inventory update for product ID: " +invTOUpdate.getProductId());
            }
        }
        return "Purchase processed";
    }

    public boolean checkOrderFeasibility(Order order){
        // Add logic for  testing if order can be fulfilled
        List<List<String>> productList = order.getProductList();
        for(List<String> product : productList) {
            String prodId = product.get(0);
            int quantity = Integer.parseInt(product.get(1));
            List<Inventory> invToCheck = inventoryRepository.findByProductId(prodId);
            boolean productAvailable = false;
            for(Inventory inv : invToCheck){
                if(inv.getQuantity() >= quantity){
                    productAvailable = true;
                    break;
                }
            }
            if(!productAvailable)
                return false;
        }
        return true;
    }

    public Order handleSellOrder(Order order){

        List<List<String>> productList = order.getProductList();
        for(List<String> product : productList){
            String prodId = product.get(0);
            int quantity = Integer.parseInt(product.get(1));

            //double price = Double.parseDouble(product.get(2));
            //double mrp = Double.parseDouble(product.get(3));
            //double discountPercent = Double.parseDouble(product.get(4));

            List<Inventory> invToUpdate = inventoryRepository.findByProductId(prodId);
            for(Inventory inv : invToUpdate){
                if(inv.getQuantity() >= quantity){
                    inv.setQuantity(inv.getQuantity() - quantity);
                    inventoryRepository.save(inv);
                    product.add(Double.toString(inv.getSellingPrice()));
                    break;
                }
            }
        }
        return order;
    }

    public String handleNewOrder(Order order){
        if(order.getType().equals("purchase")){
            order.setCreatedAt(new Timestamp(new Date().getTime()).toString());
            System.out.println("Inserting Purchase order");
            orderRepository.insert(order);
            return handlePurchaseOrder(order);
        }else if(order.getType().equals("sell")){
            if(checkOrderFeasibility(order)){
                Order orderWithSellingPrice = handleSellOrder(order);
                orderWithSellingPrice.setCreatedAt(new Timestamp(new Date().getTime()).toString());
                orderRepository.insert(orderWithSellingPrice);
                return "Sale Order Processed";

            }else
                return "Insufficient stock!";
        }else
            return "Invalid order";

    }
}
