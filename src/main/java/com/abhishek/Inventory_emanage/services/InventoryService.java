package com.abhishek.Inventory_emanage.services;

import com.abhishek.Inventory_emanage.domain.entities.Inventory;
import com.abhishek.Inventory_emanage.domain.repositories.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class InventoryService {

    @Autowired
    InventoryRepository inventoryRepository;

    public List<Inventory> getAllInventory(){
        return inventoryRepository.findAll();
    }

    public List<Inventory> findInventoryByProductId(String prodId){
        return inventoryRepository.findByProductId(prodId);
    }

    public String createInventory(Inventory inventory){
        inventory.setSellingPrice(inventory.getPurchasePrice()*(1-inventory.getDiscountPercent()/100f));
        inventory.setUpdatedAt(new Timestamp(new Date().getTime()).toString());
        Inventory insertInventory = inventoryRepository.insert(inventory);
        return "Added inventory for " + inventory.getProductId();
    }

    public String applyDiscountOnInventory(String invId, double discount){
        Optional inventoryOptional = inventoryRepository.findById(invId);
        if(inventoryOptional.isPresent()){
            Inventory inv = (Inventory) inventoryOptional.get();
            inv.setDiscountPercent(discount);
            inv.setSellingPrice(inv.getMrp() * (1 - inv.getDiscountPercent()/100.0));
            inventoryRepository.save(inv);
            return "Inventory now available with discount";
        }
        return "Inventory not found!";
    }

    public String applyDiscountOnProduct(String prodId, double discount){
        List<Inventory> prodInv = inventoryRepository.findByProductId(prodId);
        if(prodInv.isEmpty()){
            return "Product not found";
        }else {
            for (Inventory inv : prodInv) {
                inv.setDiscountPercent(discount);
                inv.setSellingPrice(inv.getMrp() * (1 - inv.getDiscountPercent()/100.0));
                inventoryRepository.save(inv);
            }
        }
        return "Product now available on discount";
    }
}
