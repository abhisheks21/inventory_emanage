package com.abhishek.Inventory_emanage.services;

import com.abhishek.Inventory_emanage.domain.entities.Inventory;
import com.abhishek.Inventory_emanage.domain.entities.Product;
import com.abhishek.Inventory_emanage.domain.repositories.InventoryRepository;
import com.abhishek.Inventory_emanage.domain.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    InventoryRepository inventoryRepository;

    public List<Product> getAllProducts(){
        return productRepository.findAll();
    }

    public Product getProductById(String prodId){
        Optional<Product> productOptional = productRepository.findById(prodId);
        return productOptional.orElseGet(() -> new Product("Product Not Found", "NA", "NA"));
    }

    public String enterNewProduct(Product product){
        Product insertProduct = productRepository.insert(product);
        return "Product entered: " + insertProduct.getpName();
    }

    public boolean checkInventoryforProductId(String prodId){
        // return true if product safe to remove and vice versa
        List<Inventory> invToCheck = inventoryRepository.findByProductId(prodId);
        if(!invToCheck.isEmpty()){
            for (Inventory inv: invToCheck) {
                if(inv.getQuantity() > 0){
                    return false;
                }
            }
        }
        return true;
    }

    public String deleteProductById(String prodId){
        Optional<Product> productOptional= productRepository.findById(prodId);
        if(productOptional.isPresent()){
            if(checkInventoryforProductId(prodId)){
                inventoryRepository.deleteAllByProductId(prodId);
                productRepository.deleteById(prodId);
                return "Deletion complete for product ID: " + prodId;
            }else{
                return "Can't remove Product. Product present in inventory";
            }
        } else{
            return "Product Not found";
        }
    }

    public String updateProduct(Product product, String prodId){
        Optional<Product> productOptional = productRepository.findById(prodId);
        if(!productOptional.isPresent())
            return "Product not found";
        else{
            Product toBeUpdated = productOptional.get();
            product.setId(toBeUpdated.getId());
            product.setCreatedAt(new Timestamp(new Date().getTime()).toString());
        }
        Product saveProduct = productRepository.save(product);
        return "Update Complete for productID:" + saveProduct.getId();
    }
}
