package com.abhishek.Inventory_emanage.controllers;

import com.abhishek.Inventory_emanage.domain.dto.InventoryDto;
import com.abhishek.Inventory_emanage.domain.entities.Inventory;
import com.abhishek.Inventory_emanage.services.InventoryService;
import org.apache.tomcat.util.json.JSONParser;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class InventoryController {

    @Autowired
    public InventoryService inventoryService;

    public final ModelMapper modelMapper = new ModelMapper();

    private InventoryDto convertToDto(Inventory inventory){
        return modelMapper.map(inventory, InventoryDto.class);
    }

    private Inventory convertToEntity(InventoryDto inventoryDto){
        return modelMapper.map(inventoryDto, Inventory.class);
    }

    @GetMapping(value = "/Inventory")
    public List<InventoryDto> getAllInventory(){
        return inventoryService.getAllInventory()
                .stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/Inventory/{prodId}")
    public List<InventoryDto> getInventoryByProductId(@PathVariable String prodId){
        return inventoryService.findInventoryByProductId(prodId)
                .stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @PutMapping(value = "/discountForInventory/{invId}/{discountString}")
    public String discountForInventory(@PathVariable String invId, @PathVariable String discountString){
        System.out.println("Discount String is" + discountString);
        double discount = Double.parseDouble(discountString);
        return inventoryService.applyDiscountOnInventory(invId, discount);
    }

    @PutMapping(value = "/discountForProduct/{prodId}/{discountString}")
    public String discountForProduct(@PathVariable String prodId, @PathVariable String discountString){
        return inventoryService.applyDiscountOnProduct(prodId, Double.parseDouble(discountString));
    }

    // NOT for use only for testing
    @PostMapping(value = "/createInventory", consumes="application/json")
    public String createInventory(@RequestBody InventoryDto inventoryDto){
        return inventoryService.createInventory(convertToEntity(inventoryDto));
    }
}
