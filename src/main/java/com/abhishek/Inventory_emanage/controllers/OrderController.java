package com.abhishek.Inventory_emanage.controllers;

import com.abhishek.Inventory_emanage.domain.dto.OrderDto;
import com.abhishek.Inventory_emanage.domain.entities.Order;
import com.abhishek.Inventory_emanage.services.OrderService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class OrderController {
    @Autowired
    OrderService orderService;

    public ModelMapper modelMapper = new ModelMapper();

    private OrderDto convertToDto(Order order){
        return modelMapper.map(order, OrderDto.class);
    }
    private Order convertToEntity(OrderDto orderDto){
        return modelMapper.map(orderDto, Order.class);
    }

    @GetMapping(value = "/Orders")
    public List<OrderDto> showAllOrders(){
        return orderService.getAllOrders()
                .stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @PostMapping(value="/newOrder", consumes = "application/json")
    public String newOrder(@RequestBody OrderDto orderDto){
        Order order = convertToEntity(orderDto);
        return orderService.handleNewOrder(order);
    }
}
