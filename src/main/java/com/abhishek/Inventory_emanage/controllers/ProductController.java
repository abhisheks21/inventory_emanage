package com.abhishek.Inventory_emanage.controllers;

import com.abhishek.Inventory_emanage.domain.dto.ProductDto;
import com.abhishek.Inventory_emanage.domain.entities.Product;
import com.abhishek.Inventory_emanage.services.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProductController {

    @Autowired
    public ProductService productService;

    public ModelMapper modelMapper = new ModelMapper();

    private ProductDto convertToDto(Product product){
        return modelMapper.map(product, ProductDto.class);
    }

    private Product convertToEntity(ProductDto productDto){
        return modelMapper.map(productDto, Product.class);
    }

    @GetMapping(value = "/Products")
    public List<ProductDto> getAllProducts(){
        List<Product> products = productService.getAllProducts();
        return products
                .stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/Products/{prodId}")
    public ProductDto getProductById(@PathVariable String prodId){
        return convertToDto(productService.getProductById(prodId));
    }

    @PostMapping(value = "/Product",consumes = "application/json")
    public String createProduct(@RequestBody ProductDto productDto){
        productDto.setCreatedAt(new Timestamp(new Date().getTime()).toString());
         return productService.enterNewProduct(convertToEntity(productDto));
    }

    @DeleteMapping(value = "/Product/{prodId}")
    public String deleteProduct(@PathVariable String prodId){
       return productService.deleteProductById(prodId);
    }

    @PutMapping(value="/Product/{prodId}")
    public String updateProduct(@RequestBody ProductDto productDto, @PathVariable String prodId){
        return productService.updateProduct(convertToEntity(productDto), prodId);
    }
}
