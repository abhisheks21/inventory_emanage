package com.abhishek.Inventory_emanage.domain.dto;

import java.util.List;

public class OrderDto {
    private String Id;
    private String type;
    private String createdAt;
    private List<List<String>> productList;

    public OrderDto() {
    }

    public OrderDto(String id, String type, String createdAt, List<List<String>> productList) {
        Id = id;
        this.type = type;
        this.createdAt = createdAt;
        this.productList = productList;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public List<List<String>> getProductList() {
        return productList;
    }

    public void setProductList(List<List<String>> productList) {
        this.productList = productList;
    }
}
