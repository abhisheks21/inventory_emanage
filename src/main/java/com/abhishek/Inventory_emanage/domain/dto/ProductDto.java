package com.abhishek.Inventory_emanage.domain.dto;

public class ProductDto {
    private String id;
    private String pName;
    private String pCategory;
    private String createdAt;

    public ProductDto(String id, String pName, String pCategory, String createdAt) {
        this.id = id;
        this.pName = pName;
        this.pCategory = pCategory;
        this.createdAt = createdAt;
    }

    public ProductDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpCategory() {
        return pCategory;
    }

    public void setpCategory(String pCategory) {
        this.pCategory = pCategory;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
