package com.abhishek.Inventory_emanage.domain.dto;

public class InventoryDto {
    private String id;
    private String productId;
    private int quantity;
    private double purchasePrice;
    private double mrp;
    private double discountPercent;
    private double sellingPrice;
    private String updatedAt;

    public InventoryDto() {
    }

    public InventoryDto(String id, String productId, int quantity, double purchasePrice, double mrp, double discountPercent, double sellingPrice, String updatedAt) {
        this.id = id;
        this.productId = productId;
        this.quantity = quantity;
        this.purchasePrice = purchasePrice;
        this.mrp = mrp;
        this.discountPercent = discountPercent;
        this.sellingPrice = sellingPrice;
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public double getMrp() {
        return mrp;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
