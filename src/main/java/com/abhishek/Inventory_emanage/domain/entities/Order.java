package com.abhishek.Inventory_emanage.domain.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "orders")
public class Order {
    @Id
    private String Id;
    private String type;
    private String createdAt;
    private List<List<String>> productList;

    @Override
    public String toString() {
        return "Order{" +
                "Id='" + Id + '\'' +
                ", type='" + type + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", productList=" + productList +
                '}';
    }

    public List<List<String>> getProductList() {
        return productList;
    }

    public void setProductList(List<List<String>> productList) {
        this.productList = productList;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
