package com.abhishek.Inventory_emanage.domain.repositories;

import com.abhishek.Inventory_emanage.domain.entities.Inventory;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoryRepository extends MongoRepository<Inventory, String> {
    List<Inventory> findByProductId(String productId);
    List<Inventory> findByProductIdAndPurchasePrice(String productId, double purchasePrice);
    void deleteAllByProductId(String productId);
}
