package com.abhishek.Inventory_emanage.domain.repositories;

import com.abhishek.Inventory_emanage.domain.entities.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends MongoRepository<Order, String> {

}
