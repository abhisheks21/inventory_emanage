package com.abhishek.Inventory_emanage;

import com.abhishek.Inventory_emanage.controllers.ProductController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;
@SpringBootTest
class InventoryEmanageApplicationTests {

	@Autowired
	private ProductController productController;
	@Test
	void contextLoads() {
		assertThat(productController).isNotNull();
	}

}
