var notesList = {};
var activeId = null;

// let noteObject = function(id, heading, body){
//     this.id = id;
//     this.heading = heading;
//     this.body = body;
// }

function createUniqueID(){
    return Intl.DateTimeFormat().resolvedOptions().timeZone + Date.now()
}

function refreshList(){
    if(localStorage.getItem("notes") === null){
        console.log("No notes found")
        return 0;
    }

    console.log("Notes stored")
    console.log(localStorage.getItem("notes"))
    let notesJSON = JSON.parse(localStorage.getItem("notes"))
    notesList = notesJSON
    console.log("Already saved notes in localstorage to refresh list   ")
    console.log(notesJSON)
    let listOfNotes = document.getElementById("listofnotes");
    listOfNotes.textContent = "";
    for(let i=notesJSON.length-1; i>=0; i--){
        let noteElement = document.createElement("li")
        noteElement.className = "noteElement"
        noteElement.setAttribute("id", notesJSON[i]["id"])
        noteElement.appendChild(document.createTextNode(notesJSON[i]["heading"])) 
        noteElement.onclick = listItemonClick
        noteElement.onblur = blurfunction;
        listOfNotes.appendChild(noteElement)
    }

}

function blurfunction(){
    // document.getElementById
}

function isArray(anyObject){
    return Object.prototype.toString.call(anyObject) === '[object Array]';
}

function deleteNote(){ 
    var storedNotes = JSON.parse(localStorage.getItem("notes"))
    for(let i=0;i<storedNotes.length;i++){
        if(storedNotes[i]["id"]=== activeId){
            storedNotes.splice(i,1);
            localStorage.setItem("notes", JSON.stringify(storedNotes))
            break;
        }
    }
    refreshList();
}

function saveNote(heading, body){
    if(activeId !== null){
        var storedNotes = JSON.parse(localStorage.getItem("notes"))
        // console.log("stored arr")
        // console.log(storedNotes)
        for(let x=0;x<storedNotes.length;x++){
            if(storedNotes[x]["id"] === activeId){
                storedNotes[x]["heading"] = heading
                storedNotes[x]["body"] = body
                localStorage.setItem("notes", JSON.stringify(storedNotes))
                break;
            }
        }
        refreshList();
    }
    else{
        let id = createUniqueID()
        let note = {
            "id" : id,
            "heading": heading,
            "body": body   
        }

        if(localStorage.getItem("notes") !== null && isArray(JSON.parse(localStorage.getItem("notes")))){
            var storedArr = JSON.parse(localStorage.getItem("notes"))
            console.log("stored arr")
            console.log(storedArr)
            // localStorage.notes = JSON.stringify().push(note))
            // storedArr = storedArr.push(note)
            storedArr.push(note)
            localStorage.setItem("notes", JSON.stringify(storedArr))
        }else{
            localStorage.setItem("notes",JSON.stringify([note]))
        }
        refreshList();
        document.getElementById("editarea").style.display = "none"
        document.getElementById("selectanote").style.display = "grid"
    }  
}

function listItemonClick(event){
    console.log("list item clicked")
    activeId = event.target.id;
    console.log(activeId)
    let i = null;
    for(let j = 0; j<notesList.length;j++){
        if(notesList[j]["id"] ===  activeId){
            console.log("active id is "+activeId)
            console.log("activeid found at"+ j)
            i = j;
            break;
        }
    }
    let currButton = document.getElementById(activeId);
    currButton.style.backgroundColor = "#7DD181";
    // document.getElementById(activeId).style.backgroundColor = "#96E8BC";
    

    console.log(notesList[i]["heading"])
    document.getElementById("noteheading").value = notesList[i]["heading"]
    console.log(notesList[i]["body"])
    document.getElementById("notebody").value = notesList[i]["body"]
    document.getElementById("editarea").style.display = "flex"
    // document.getElementById("#noteheading").style.flexGrow = 0.5
    // document.getElementById("#notebody").style.flexGrow = 20
    // document.getElementById("#footerbuttons").style.flexGrow = 0.5
    document.getElementById("selectanote").style.display = "none"
}

    // if(listOfNotes.childNodes.length > 0){
    //     listOfNotes.insertBefore(noteElement, listOfNotes.childNodes[0])
    // }else{
    //     listOfNotes.appendChild(noteElement);
    // }
    
var addButton = document.getElementById("addnote")
addButton.addEventListener("click", event => {
    // console.log("clicked")
    document.getElementById("editarea").style.display = "flex"
    document.getElementById("selectanote").style.display = "none"
    document.getElementById("noteheading").value = ""
    document.getElementById("notebody").value = ""
    // document.getElementById("#noteheading").style.flexGrow = 0.5
    // document.getElementById("#notebody").style.flexGrow = 20
    // document.getElementById("#footerbuttons").style.flexGrow = 0.5
    activeId = null;
});


window.onload = refreshList;

document.getElementById("savebutton").onclick = () => {
    saveNote(document.getElementById("noteheading").value, document.getElementById("notebody").value)
    document.getElementById("editarea").style.display = "none"
    document.getElementById("selectanote").style.display = "grid"
}

document.getElementById("deletebutton").onclick = () => {
    deleteNote();
    document.getElementById("editarea").style.display = "none"
    document.getElementById("selectanote").style.display = "grid"
}



// document.getElementById("deletebutton").onclick = deleteNote;